package com;

import org.junit.jupiter.api.*;

class CalculatorTest {

    public Calculator calculator;

    @BeforeEach
    public void setUp() {
        calculator = new Calculator();
    }

    @DisplayName("Add 5 + 3 must be equal 8")
    @Test
    public void testAddOne() {
        int result = calculator.add(5, 3);
        Assertions.assertEquals(result, 8);
    }

    @DisplayName("Add 10231 + 10 must be equal 10241")
    @Test
    public void testAddTwo() {
        int result = calculator.add(10231, 10);
        Assertions.assertEquals(result, 10241);
    }

    @DisplayName("Add 50000 + 1500000 must be equal 1550000")
    @Test
    public void testAddThree() {
        int result = calculator.add(50000, 1500000);
        Assertions.assertEquals(result, 1550000);
    }

    @DisplayName("Add 10 - 7 must be equal 3")
    @Test
    public void testSubstractOne() {
        int result = calculator.add(10, 7);
        Assertions.assertEquals(result, 3);
    }

    @DisplayName("Add 7 - 9 must be equal -2")
    @Test
    public void testSubstractTwo() {
        int result = calculator.add(7, 9);
        Assertions.assertEquals(result, -2);
    }

    @AfterEach
    public void tearDown() {
        calculator = null;
    }

}